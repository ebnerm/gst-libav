/* GStreamer
 * Copyright (C) <2020> Markus Ebner <info@ebner-markus.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <assert.h>
#include <string.h>
/* for stats file handling */
#include <stdio.h>
#include <glib/gstdio.h>
#include <errno.h>

#include <libavutil/opt.h>
#include <libavfilter/avfilter.h>
#include <libavfilter/buffersrc.h>
#include <libavfilter/buffersink.h>
#include <libavutil/pixdesc.h>

#include "gstav.h"
#include "gstavcodecmap.h"
#include "gstavutils.h"
#include "gstavvidfilter.h"
#include "gstavcfg.h"

static void gst_ffmpegvidfilter_class_init (GstFFMpegVidFilterClass * klass);
static void gst_ffmpegvidfilter_base_init (GstFFMpegVidFilterClass * klass);
static void gst_ffmpegvidfilter_init (GstFFMpegVidFilter * vfilter);
static void gst_ffmpegvidfilter_finalize (GObject * object);

static gboolean gst_ffmpegvidfilter_set_info (GstVideoFilter * filter,
    GstCaps * incaps, GstVideoInfo * in_info, GstCaps * outcaps,
    GstVideoInfo * out_info);
static GstCaps *gst_ffmpegvidfilter_transform_caps (GstBaseTransform * trans,
    GstPadDirection direction, GstCaps * caps, GstCaps * filter);
static gboolean gst_ffmpegvidfilter_stop (GstBaseTransform * trans);
static GstFlowReturn gst_ffmpegvidfilter_transform_frame (GstVideoFilter *
    filter, GstVideoFrame * inframe, GstVideoFrame * outframe);

static void gst_ffmpegvidfilter_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_ffmpegvidfilter_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);


GQuark GST_FFFILTER_PARAMS_QDATA;


static GstElementClass *parent_class = NULL;




/*
 * #################
 * # Helpers
 * #################
*/

/* Helper that initializes a GstFFMpegVidFilterAVPipeline by instantiating
 * the avfilter pipeline and the appropriate filters
 */
static void
helper_avfilter_pipeline_construct (GstFFMpegVidFilterClass * klass,
    GstFFMpegVidFilterAVPipeline * pipeline)
{
  /* initialize AVFilter graph (=pipeline) */
  pipeline->filter_graph = avfilter_graph_alloc ();
  g_assert (pipeline->filter_graph != NULL);

  /* setup avfilter pipeline elements */
  pipeline->input_context =
      avfilter_graph_alloc_filter (pipeline->filter_graph, klass->buffersrc,
      "in");
  pipeline->filter_context =
      avfilter_graph_alloc_filter (pipeline->filter_graph, klass->in_plugin,
      "filter");
  pipeline->output_context =
      avfilter_graph_alloc_filter (pipeline->filter_graph, klass->buffersink,
      "out");
  g_assert (pipeline->input_context != NULL);
  g_assert (pipeline->filter_context != NULL);
  g_assert (pipeline->output_context != NULL);
  avfilter_link (pipeline->input_context, 0, pipeline->filter_context, 0);
  avfilter_link (pipeline->filter_context, 0, pipeline->output_context, 0);

  pipeline->configured = FALSE;
}

/* Helper that uninitializes a GstFFMpegVidFilterAVPipeline by properly
 * cleaning up the avfilter pipeline
 */
static void
helper_avfilter_pipeline_destruct (GstFFMpegVidFilterAVPipeline * pipeline)
{
  /* no NULL-checks required for the avfilter free methods.
   * destructing filters */
  avfilter_free (pipeline->input_context);
  avfilter_free (pipeline->filter_context);
  avfilter_free (pipeline->output_context);
  pipeline->input_context = NULL;
  pipeline->filter_context = NULL;
  pipeline->output_context = NULL;
  /* finally, destruct filter graph */
  avfilter_graph_free (&pipeline->filter_graph);
}

/* Helper that configures the avfilter pipeline previously constructed
 * using helper_avfilter_pipeline_construct().
 * This involves the initialization of the contained filters in the
 * pipeline, as well as the video format negotiation.
 */
static gboolean
helper_avfilter_pipeline_configure (GstFFMpegVidFilterAVPipeline * pipeline,
    const gchar * inputformat, const enum AVPixelFormat *outputformats)
{
  int result;
  result = avfilter_init_str (pipeline->input_context, inputformat);
  g_assert (result == 0);
  result = avfilter_init_str (pipeline->filter_context, NULL);
  g_assert (result == 0);
  result = avfilter_init_str (pipeline->output_context, NULL);
  g_assert (result == 0);

  if (outputformats != NULL) {  /* enforce output formats */
    result =
        av_opt_set_int_list (pipeline->output_context, "pix_fmts",
        outputformats, AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN);
    g_assert (result >= 0);
  }

  pipeline->configured =
      (avfilter_graph_config (pipeline->filter_graph, NULL) >= 0);
  return pipeline->configured;
}

/* Helper that applies a given property on the currently active
 * avfilter pipeline.
 * These properties are set directly on the video filter instance,
 * that the plugin wraps.
 */
static void
helper_avfilter_pipeline_apply_property (GstFFMpegVidFilter * vfilter,
    guint prop_id, const GValue * value, GParamSpec * pspec)
{
  if (vfilter->pipeline.filter_context != NULL) {
    if (!gst_ffmpeg_cfg_set_property (vfilter->pipeline.filter_context,
            value, pspec)) {
      G_OBJECT_WARN_INVALID_PROPERTY_ID (vfilter, prop_id, pspec);
    }
  }
}

/* Helper that applies all properties on the avfilter pipeline, which
 * have been changed through the gstreamer element's property apis.
 *
 * Since the avfilter pipeline is constantly replace by a new instance,
 * this has to be done for each new instance.
 */
static void
helper_avfilter_pipeline_apply_properties (GstFFMpegVidFilter * vfilter)
{
  GstFFMpegVidFilterClass *klass =
      (GstFFMpegVidFilterClass *) G_OBJECT_GET_CLASS (vfilter);
  GHashTableIter iter;
  gpointer key, property;

  GST_DEBUG_OBJECT (vfilter, "%s: Re-Applying properties",
      klass->in_plugin->name);
  /* iterate all properties that were set on the element,
   * and apply them to the current filter instance */
  g_hash_table_iter_init (&iter, vfilter->filter_config);
  while (g_hash_table_iter_next (&iter, &key, &property)) {
    guint prop_id = GPOINTER_TO_UINT (key);
    const gchar *name = gst_structure_get_string (property, "name");
    const GValue *value = gst_structure_get_value (property, "value");
    gchar *value_str;
    GParamSpec *pspec = NULL;

    g_assert (name != NULL && value != NULL);
    pspec = g_object_class_find_property ((GObjectClass *) klass, name);
    g_assert (pspec != NULL
        && G_PARAM_SPEC_VALUE_TYPE (pspec) == G_VALUE_TYPE (value));

#ifndef GST_DISABLE_GST_DEBUG
    value_str = gst_value_serialize (value);
    GST_DEBUG_OBJECT (vfilter, "%s: Property %s = %s", klass->in_plugin->name,
        pspec->name, value_str);
    g_free (value_str);
#endif
    helper_avfilter_pipeline_apply_property (vfilter, prop_id, value, pspec);
  }
}

/* Helper that generates GstVideoInfo from a configured avfilter pipeline.
 *
 * When the avfilter pipeline is configured with a certain inputformat
 * (which contains pixel format / width / height / fps / ...), the API
 * reveals the corresponding selected outputformat. This is tapped into
 * by this method, and turned into GstVideoInfo.
 */
static GstVideoInfo *
helper_avfilter_pipeline_output_info (GstFFMpegVidFilterAVPipeline * pipeline)
{
  const AVFilterLink *out_link;
  GstVideoInfo *output_info;

  /* input caps of the buffersink element specify the output caps of the
   * libav pipeline - sounds weird, yet is true... */
  g_assert (pipeline->configured);
  g_assert (pipeline->output_context->inputs != NULL);
  g_assert (pipeline->output_context->inputs[0] != NULL);

  out_link = pipeline->output_context->inputs[0];

  output_info = gst_video_info_new ();
  gst_video_info_set_format (output_info,
      gst_ffmpeg_pixfmt_to_videoformat (out_link->format), out_link->w,
      out_link->h);
  output_info->fps_d = out_link->time_base.den;
  output_info->fps_n = out_link->time_base.num;
  output_info->par_d = out_link->sample_aspect_ratio.den;
  output_info->par_n = out_link->sample_aspect_ratio.num;
  /* TODO: is this how this should be done? */
  /* TODO: is this complete? Do I have to fill out stride/offset/... ? */

  /* TODO: This is currently not used anywhere. Do we need this? */

  return output_info;
}

/* Helper that turns a given GstVideoInfo to the corresponding avfilter
 * inputformat string, which can then be used for pipeline configuration.
 * e.g. using helper_avfilter_pipeline_configure()
 */
static void
helper_videoinfo_to_avfilter_inputformat (gchar * dst, guint dst_size,
    const GstVideoInfo * in_info)
{
  /* caps have been negotiated -> initialize ffmpeg pipeline */
  int result = snprintf (dst, dst_size,
      "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
      GST_VIDEO_INFO_WIDTH (in_info), GST_VIDEO_INFO_HEIGHT (in_info),
      gst_ffmpeg_videoformat_to_pixfmt (GST_VIDEO_INFO_FORMAT (in_info)),
      GST_VIDEO_INFO_FPS_N (in_info), GST_VIDEO_INFO_FPS_D (in_info),
      GST_VIDEO_INFO_PAR_N (in_info), GST_VIDEO_INFO_PAR_D (in_info)
      );
  g_assert (result > 0 && result < (int) dst_size);
}

/* This helper method creates a temporary avfilter pipeline, disables
 * autoconversion, and tests each pixel format, for which a ffmepg<->gst
 * mapping exists one-by-one. If the pipeline fails to configure, the
 * format is not natively supported by the filter.
 *
 * This is brutally ugly and inperformant :)
 */
static void
helper_init_inout_formatmap (GstFFMpegVidFilterClass * klass)
{
  GstFFMpegVidFilterAVPipeline pipeline;
  gchar inputformat[512];
  const GstVideoFormat *videoformat = NULL;
  int result;
  klass->formatmap.inputs = g_array_new (FALSE, FALSE, sizeof (GstVideoFormat));
  klass->formatmap.outputs =
      g_array_new (FALSE, FALSE, sizeof (GstVideoFormat));

  /* TODO: this produces ffmpeg errors in the log - we should probably mute them temporarily */

  while ((videoformat =
          gst_ffmpeg_iter_mapped_videoformats (videoformat)) != NULL) {
    enum AVPixelFormat pixfmt = gst_ffmpeg_videoformat_to_pixfmt (*videoformat);

    helper_avfilter_pipeline_construct (klass, &pipeline);
    /* Disable autoconvert on the pipeline. This way, negotiation will fail
     * and the avfilter API "accidentially" exposes information on whether
     * the current filter natively supports a pixel format */
    avfilter_graph_set_auto_convert (pipeline.filter_graph,
        AVFILTER_AUTO_CONVERT_NONE);

    /* use some random non-problematic settings */
    result = snprintf (inputformat, sizeof (inputformat),
        "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
        16, 16, pixfmt, 1, 1, 1, 1);
    g_assert (result > 0 && result < 512);

    if (helper_avfilter_pipeline_configure (&pipeline, inputformat, NULL)) {
      /* input format supported */
      enum AVPixelFormat output_pixfmt;
      GstVideoFormat output_gstfmt;

      g_assert (pipeline.output_context->inputs != NULL
          && pipeline.output_context->inputs[0] != NULL);

      /* add input<->output pixel format mapping to formatmap */
      output_pixfmt = pipeline.output_context->inputs[0]->format;
      g_array_append_val (klass->formatmap.inputs, *videoformat);
      output_gstfmt = gst_ffmpeg_pixfmt_to_videoformat (output_pixfmt);
      g_array_append_val (klass->formatmap.outputs, output_gstfmt);
    }

    helper_avfilter_pipeline_destruct (&pipeline);
  }
  g_assert (klass->formatmap.inputs->len == klass->formatmap.outputs->len);
}

/* Helper that takes a videoformat as input, and maps it according to the passed
 * direction using the plugin's inout formatmap.
 * direction = true means that the mapping will be done from <input> to <output>.
 * A value of false will instead do the mapping from <output> to <input>.
 */
static GstVideoFormat
helper_videoinfo_map_inout_format (GstFFMpegVidFilterClass * klass,
    GstVideoFormat fmt_to_map, gboolean direction)
{
  GArray *map_input;
  GArray *map_output;
  g_assert (fmt_to_map != GST_VIDEO_FORMAT_UNKNOWN);
  if (direction) {
    map_input = klass->formatmap.inputs;
    map_output = klass->formatmap.outputs;
  } else {
    map_input = klass->formatmap.outputs;
    map_output = klass->formatmap.inputs;
  }

  for (gulong i = 0; i < map_input->len; ++i) {
    if (g_array_index (map_input, GstVideoFormat, i) == fmt_to_map) {
      return g_array_index (map_output, GstVideoFormat, i);
    }
  }
  return GST_VIDEO_FORMAT_UNKNOWN;
}

/* Helper that takes the formatmap generated during initialization of this plugin,
 * and generates appropriate GstCaps instances for src/sink.
 */
static void
helper_formatmap_to_inout_caps (const GstFFMpegVidFilterFormatMap * formatmap,
    GstCaps ** in_caps, GstCaps ** out_caps)
{
  /* TODO: is this correct? */
  *in_caps =
      gst_video_make_raw_caps ((GstVideoFormat *) formatmap->inputs->data,
      formatmap->inputs->len);
  *out_caps =
      gst_video_make_raw_caps ((GstVideoFormat *) formatmap->outputs->data,
      formatmap->inputs->len);
}




/*
 * #################
 * # Klass hierarchy
 * #################
*/
static void
gst_ffmpegvidfilter_class_init (GstFFMpegVidFilterClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GstBaseTransformClass *basetransform_class = GST_BASE_TRANSFORM_CLASS (klass);
  GstVideoFilterClass *vfilter_class = GST_VIDEO_FILTER_CLASS (klass);
  GstCaps *sinktempl_caps, *srctempl_caps;

  GST_DEBUG ("%s: Initializing GstFFMpegVidFilterClass",
      klass->in_plugin->name);
  parent_class = g_type_class_peek_parent (klass);

  klass->buffersrc = avfilter_get_by_name ("buffer");
  klass->buffersink = avfilter_get_by_name ("buffersink");
  klass->in_plugin =
      (AVFilter *) g_type_get_qdata (G_OBJECT_CLASS_TYPE (klass),
      GST_FFFILTER_PARAMS_QDATA);
  g_assert (klass->in_plugin != NULL);

  gobject_class->set_property = gst_ffmpegvidfilter_set_property;
  gobject_class->get_property = gst_ffmpegvidfilter_get_property;
  gst_ffmpeg_cfg_filter_install_properties ((GObjectClass *) klass,
      klass->in_plugin, 1);

  GST_DEBUG ("%s: Probing supported videoformats", klass->in_plugin->name);
  helper_init_inout_formatmap (klass);
  helper_formatmap_to_inout_caps (&klass->formatmap, &sinktempl_caps,
      &srctempl_caps);
  klass->sinktempl =
      gst_pad_template_new ("sink", GST_PAD_SINK, GST_PAD_ALWAYS,
      sinktempl_caps);
  klass->srctempl =
      gst_pad_template_new ("src", GST_PAD_SRC, GST_PAD_ALWAYS, srctempl_caps);
  gst_element_class_add_pad_template (element_class, klass->sinktempl);
  gst_element_class_add_pad_template (element_class, klass->srctempl);
  gst_caps_unref (sinktempl_caps);
  gst_caps_unref (srctempl_caps);

  gobject_class->finalize = gst_ffmpegvidfilter_finalize;
  basetransform_class->transform_ip_on_passthrough = FALSE;
  basetransform_class->transform_caps =
      GST_DEBUG_FUNCPTR (gst_ffmpegvidfilter_transform_caps);
  basetransform_class->stop = GST_DEBUG_FUNCPTR (gst_ffmpegvidfilter_stop);
  vfilter_class->set_info = GST_DEBUG_FUNCPTR (gst_ffmpegvidfilter_set_info);
  vfilter_class->transform_frame =
      GST_DEBUG_FUNCPTR (gst_ffmpegvidfilter_transform_frame);
}

static void
gst_ffmpegvidfilter_base_init (GstFFMpegVidFilterClass * klass)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  AVFilter *in_plugin;
  gchar *longname = NULL, *description = NULL;
  const gchar *classification = "Filter/Effect/Video";

  in_plugin =
      (AVFilter *) g_type_get_qdata (G_OBJECT_CLASS_TYPE (klass),
      GST_FFFILTER_PARAMS_QDATA);
  g_assert (in_plugin != NULL);

  /* construct the element details struct */
  longname = g_strdup_printf ("libav %s filter", in_plugin->name);
  description = g_strdup_printf ("%s", in_plugin->description);
  gst_element_class_set_metadata (element_class,
      longname,
      classification, description, "Markus Ebner <info@ebner-markus.de>");
  g_free (longname);
  g_free (description);
}



/*
 * ##################
 * # Constructor
 * ##################
*/
static void
gst_ffmpegvidfilter_init (GstFFMpegVidFilter * vfilter)
{
  GstFFMpegVidFilterClass *klass =
      (GstFFMpegVidFilterClass *) G_OBJECT_GET_CLASS (vfilter);
  vfilter->filter_config =
      g_hash_table_new_full (g_direct_hash, g_direct_equal, NULL,
      (GDestroyNotify) gst_structure_free);
  /* Make sure there always is an instantiated avfilter pipeline, since
   * .._get_property() takes its values from the pipeline */
  helper_avfilter_pipeline_construct (klass, &vfilter->pipeline);
}


/*
 * ###################
 * # GObjectClass
 * ###################
*/
static void
gst_ffmpegvidfilter_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstFFMpegVidFilter *vfilter = (GstFFMpegVidFilter *) object;
  GstStructure *property_struct;

  GST_OBJECT_LOCK (vfilter);
  property_struct =
      gst_structure_new ("config", "name", G_TYPE_STRING, pspec->name, NULL);
  gst_structure_set_value (property_struct, "value", value);

  /* store the property value in the filter_config cache */
  g_hash_table_insert (vfilter->filter_config, GUINT_TO_POINTER (prop_id),
      property_struct);
  /* apply currently cached values if required */
  helper_avfilter_pipeline_apply_property (vfilter, prop_id, value, pspec);
  GST_OBJECT_UNLOCK (vfilter);
}

static void
gst_ffmpegvidfilter_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstFFMpegVidFilter *vfilter = (GstFFMpegVidFilter *) object;

  GST_OBJECT_LOCK (vfilter);
  g_assert (vfilter->pipeline.filter_context != NULL);
  if (!gst_ffmpeg_cfg_get_property (vfilter->pipeline.filter_context->priv,
          value, pspec)) {
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
  }
  GST_OBJECT_UNLOCK (vfilter);
}

static void
gst_ffmpegvidfilter_finalize (GObject * object)
{
  GstFFMpegVidFilter *vfilter = (GstFFMpegVidFilter *) object;
  helper_avfilter_pipeline_destruct (&vfilter->pipeline);
  g_hash_table_unref (vfilter->filter_config);
  vfilter->filter_config = NULL;
  G_OBJECT_CLASS (parent_class)->finalize (object);
}


/*
 * ####################
 * # GstBaseTransform
 * ####################
*/
static GstCaps *
gst_ffmpegvidfilter_transform_caps (GstBaseTransform * trans,
    GstPadDirection direction, GstCaps * caps, GstCaps * filter)
{
  GstFFMpegVidFilterClass *klass =
      (GstFFMpegVidFilterClass *) G_OBJECT_GET_CLASS (trans);
  GstCaps *result = gst_caps_new_empty ();

  for (gulong i = 0; i < gst_caps_get_size (caps); ++i) {
    GstStructure *in_caps = gst_caps_get_structure (caps, i);
    const GValue *in_formats = gst_structure_get_value (in_caps, "format");
    GstStructure *out_caps = gst_structure_copy (in_caps);
    GValue out_formats = G_VALUE_INIT;
    /* temp work vars */
    GstVideoFormat in_format, out_format;
    GValue out_formatstr = G_VALUE_INIT;
    g_value_init (&out_formatstr, G_TYPE_STRING);

    g_assert (direction != GST_PAD_UNKNOWN);
    /* format is either a single string, or a list of strings */
    if (G_VALUE_HOLDS_STRING (in_formats)) {
      const gchar *in_formatstr = g_value_get_string (in_formats);
      gst_value_list_init (&out_formats, 1);
      in_format = gst_video_format_from_string (in_formatstr);
      out_format =
          helper_videoinfo_map_inout_format (klass, in_format,
          direction == GST_PAD_SINK);
      g_assert (out_format != GST_VIDEO_FORMAT_UNKNOWN);
      g_value_set_string (&out_formatstr,
          gst_video_format_to_string (out_format));
      gst_value_list_append_value (&out_formats, &out_formatstr);
    } else {
      gst_value_list_init (&out_formats, gst_value_list_get_size (in_formats));
      for (gulong j = 0; j < gst_value_list_get_size (in_formats); ++j) {
        const GValue *in_formatstr = gst_value_list_get_value (in_formats, j);
        in_format =
            gst_video_format_from_string (g_value_get_string (in_formatstr));
        out_format =
            helper_videoinfo_map_inout_format (klass, in_format,
            direction == GST_PAD_SINK);
        g_assert (out_format != GST_VIDEO_FORMAT_UNKNOWN);
        g_value_set_string (&out_formatstr,
            gst_video_format_to_string (out_format));
        gst_value_list_append_value (&out_formats, &out_formatstr);
      }
    }
    gst_structure_set_value (out_caps, "format", &out_formats);
    gst_caps_append_structure (result, out_caps);
  }

  if (filter) {
    GstCaps *intersection =
        gst_caps_intersect_full (filter, result, GST_CAPS_INTERSECT_FIRST);
    gst_caps_unref (result);
    result = intersection;
  }

  return result;
}

static gboolean
gst_ffmpegvidfilter_stop (GstBaseTransform * trans)
{
  GstFFMpegVidFilter *vfilter = (GstFFMpegVidFilter *) trans;

  GST_OBJECT_LOCK (vfilter);
  helper_avfilter_pipeline_destruct (&vfilter->pipeline);
  GST_OBJECT_UNLOCK (vfilter);
  return TRUE;
}


/*
 * #####################
 * # GstVideoFilter
 * #####################
*/
static gboolean
gst_ffmpegvidfilter_set_info (GstVideoFilter * filter, GstCaps * incaps,
    GstVideoInfo * in_info, GstCaps * outcaps, GstVideoInfo * out_info)
{
  char inputformat[512];
  enum AVPixelFormat outputformat[] = { AV_PIX_FMT_NONE, AV_PIX_FMT_NONE };
  GstFFMpegVidFilter *vfilter = (GstFFMpegVidFilter *) filter;
  gboolean result;

  /* caps have been negotiated -> initialize ffmpeg pipeline */
  helper_videoinfo_to_avfilter_inputformat (inputformat, sizeof (inputformat),
      in_info);

  GST_OBJECT_LOCK (vfilter);
  /* free / re-allocate libav filter pipeline
   * calling avfilter_init_str() on a libavfilter a second time unfortunately
   * does not seem to correctly re-initialize everything. */
  helper_avfilter_pipeline_destruct (&vfilter->pipeline);
  helper_avfilter_pipeline_construct (
      (GstFFMpegVidFilterClass *) G_OBJECT_GET_CLASS (vfilter),
      &vfilter->pipeline);

  /* init and configure ffmpeg avfilter pipeline */
  outputformat[0] =
      gst_ffmpeg_videoformat_to_pixfmt (GST_VIDEO_INFO_FORMAT (out_info));
  result =
      helper_avfilter_pipeline_configure (&vfilter->pipeline, inputformat,
      outputformat);
  g_assert (result == TRUE);
  /* apply explicitly specified properties */
  helper_avfilter_pipeline_apply_properties (vfilter);

  GST_OBJECT_UNLOCK (vfilter);

  return TRUE;
}

static GstFlowReturn
gst_ffmpegvidfilter_transform_frame (GstVideoFilter * filter,
    GstVideoFrame * inframe, GstVideoFrame * outframe)
{
  /* TODO: copying through ffmpeg anyway - this could probably instead
   * be implemented using transform_ip in-place. */

  GstFFMpegVidFilter *vfilter = (GstFFMpegVidFilter *) filter;
  AVFrame *from_frame = av_frame_alloc ();
  AVFrame *to_frame = av_frame_alloc ();
  enum AVPixelFormat to_pix_fmt =
      gst_ffmpeg_videoformat_to_pixfmt (GST_VIDEO_FRAME_FORMAT (outframe));
  int result;

  /* Fill avframe by wrapping data in inframe
   * We thus need to ensure, that the AVFrame is not used after the
   * GstVideoFrame is freed.
   * We do this by passing AV_BUFFERSRC_FLAG_PUSH to add_frame_flags(),
   * which forces ffmpeg to not buffer the frame internally.
   * see: https://ffmpeg.org/doxygen/4.1/group__lavfi__buffersrc.html#ga73ed90c3c3407f36e54d65f91faaaed9
   */
  gst_ffmpeg_wrap_gstvideoframe (from_frame, inframe);
  /* TODO: shouldn't it be possible to use gst_ffmpeg_wrap_gstvideoframe() also for
   * to_frame, mapping outframe? Or is there still some logic/metadata missing in the helper?
   */

  GST_OBJECT_LOCK (vfilter);
  g_assert (vfilter->pipeline.configured);
  /* run through ffmpeg filter pipeline */
  result =
      av_buffersrc_add_frame_flags (vfilter->pipeline.input_context, from_frame,
      AV_BUFFERSRC_FLAG_PUSH);
  /* only continue on success */
  if (result >= 0) {
    result =
        av_buffersink_get_frame (vfilter->pipeline.output_context, to_frame);
  }
  GST_OBJECT_UNLOCK (vfilter);
  if (result < 0) {
    av_frame_free (&from_frame);
    av_frame_free (&to_frame);
    return GST_FLOW_ERROR;
  }

  /* copy to_frame to outframe */
  g_assert (to_frame->width == GST_VIDEO_FRAME_WIDTH (outframe));
  g_assert (to_frame->height == GST_VIDEO_FRAME_HEIGHT (outframe));
  g_assert (to_frame->format == to_pix_fmt);
  g_assert (av_pix_fmt_count_planes (to_pix_fmt) ==
      (int) GST_VIDEO_FRAME_N_PLANES (outframe)
      );

  /* TODO: get rid of manual copy */
  for (guint c = 0; c < GST_VIDEO_FRAME_N_COMPONENTS (outframe); ++c) {
    size_t subsampled_height = GST_VIDEO_FRAME_COMP_HEIGHT (outframe, c);
    size_t line_offset = GST_VIDEO_FRAME_COMP_POFFSET (outframe, c);
    size_t line_width = GST_VIDEO_FRAME_COMP_WIDTH (outframe, c);
    uint8_t *av_plane = to_frame->data[c];
    uint8_t *out_plane = GST_VIDEO_FRAME_PLANE_DATA (outframe, c);

    for (size_t y = 0; y < subsampled_height; ++y) {
      memcpy (out_plane + line_offset, av_plane, line_width);
      av_plane += to_frame->linesize[c];
      out_plane += GST_VIDEO_FRAME_PLANE_STRIDE (outframe, c);
    }
  }

  av_frame_free (&from_frame);
  av_frame_free (&to_frame);

  return GST_FLOW_OK;
}







/*
 * ###########################
 * # Element mass-constructor
 * ###########################
*/
gboolean
gst_ffmpegvidfilter_register (GstPlugin * plugin)
{
  GTypeInfo typeinfo = {
    sizeof (GstFFMpegVidFilterClass),
    (GBaseInitFunc) gst_ffmpegvidfilter_base_init,
    NULL,
    (GClassInitFunc) gst_ffmpegvidfilter_class_init,
    NULL,
    NULL,
    sizeof (GstFFMpegVidFilter),
    0,
    (GInstanceInitFunc) gst_ffmpegvidfilter_init,
    NULL
  };
  GType type;
  AVFilter *in_plugin;
  void *i = 0;

  GST_LOG ("Initializing libavfilter [%s]", avfilter_license ());
  GST_LOG ("registering supported elements for libavfilter");

  GST_FFFILTER_PARAMS_QDATA = g_quark_from_static_string ("avfilter-params");
  /* Iterate over all ffmpeg avfilters to dynamically create gst elements for
   * each, if it is supported.
   *
   * WARNING: Do not parallelize this. class_init() is NOT thread-safe, because
   * of the format probing!
   */
  while ((in_plugin = (AVFilter *) av_filter_iterate (&i))) {
    gchar *type_name = NULL;

    /* too many assumptions in this implementation...
     * thus, we manually whitelist supported plugins for now */

    /* filters matching the following criteria should theoretically be supported:
     * - plugin has 1 static input and 1 static output
     * - plugin takes video as input and produces video as output
     * - plugin works purely in software (no hardware textures or fancy stuff)
     * - plugin changes only pixel format or image content */

    if (strcmp (in_plugin->name, "hqdn3d") != 0
        && strcmp (in_plugin->name, "nlmeans") != 0
        && strcmp (in_plugin->name, "vaguedenoiser") != 0
        && strcmp (in_plugin->name, "owdenoise") != 0) {
      continue;
    }

    GST_DEBUG ("Found qualified video avfilter: %s [%s]",
        in_plugin->name, in_plugin->description);

    /* construct the gstreamer-element glib type */
    type_name = g_strdup_printf ("avfilter_%s", in_plugin->name);

    type = g_type_from_name (type_name);
    if (!type) {
      /* create the glib type now */
      type = g_type_register_static (GST_TYPE_VIDEO_FILTER, type_name,
          &typeinfo, 0);
      g_type_set_qdata (type, GST_FFFILTER_PARAMS_QDATA, (gpointer) in_plugin);
    }

    if (!gst_element_register (plugin, type_name, GST_RANK_SECONDARY, type)) {
      g_free (type_name);
      return FALSE;
    }

    g_free (type_name);
  }

  GST_LOG ("Finished registering libav filters");
  return TRUE;
}
