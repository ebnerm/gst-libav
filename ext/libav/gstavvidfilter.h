/* GStreamer
 * Copyright (C) <2020> Markus Ebner <info@ebner-markus.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/* First, include the header file for the plugin, to bring in the
 * object definition and other useful things.
 */

#ifndef __GST_FFMPEGVIDFILTER_H__
#define __GST_FFMPEGVIDFILTER_H__

#include <gst/gst.h>
#include <gst/video/video.h>
#include <libavfilter/avfilter.h>

typedef struct GstFFMpegVidFilterAVPipeline
{
  AVFilterGraph *filter_graph;
  AVFilterContext *input_context;
  AVFilterContext *filter_context;
  AVFilterContext *output_context;

  /* specifies whether the contained avfilter instances have
   * been configured (with pixel format / width / height / etc..)
   */
  gboolean configured;
} GstFFMpegVidFilterAVPipeline;

typedef struct GstFFMpegVidFilterFormatMap
{
  GArray *inputs;
  GArray *outputs;
} GstFFMpegVidFilterFormatMap;

G_BEGIN_DECLS typedef struct _GstFFMpegVidFilter GstFFMpegVidFilter;
struct _GstFFMpegVidFilter
{
  GstVideoFilter parent;

  /* ffmpeg filter pipeline */
  GstFFMpegVidFilterAVPipeline pipeline;

  /* the employed filter pipeline has to be destroyed / recreated
   * when the format changes. Applied properties thus have to be
   * stored outside of the actual filter instance, and be re-applied
   * when the filter is instantiated...
   */
  GHashTable *filter_config;
};

typedef struct _GstFFMpegVidFilterClass GstFFMpegVidFilterClass;
struct _GstFFMpegVidFilterClass
{
  GstVideoFilterClass parent_class;

  /* ffmpeg AVFilter classes used in the employed libavfilter pipeline */
  const AVFilter *buffersrc;
  const AVFilter *in_plugin;
  const AVFilter *buffersink;

  /* List of mappings between input pixel formats, and their corresponding
   * output pixel formats, for the employed AVFilter.
   */
  GstFFMpegVidFilterFormatMap formatmap;

  /* pad templates */
  GstPadTemplate *srctempl, *sinktempl;
};

G_END_DECLS
#endif /* __GST_FFMPEGVIDFILTER_H__ */
